// Definitions partly from https://github.com/Microsoft/TypeScript/issues/28308#issuecomment-650802278
interface AudioWorkletProcessor {
    readonly port: MessagePort;
    process(
        inputs: Float32Array[][],
        outputs: Float32Array[][],
        parameters: Record<string, Float32Array>
    ): boolean;
}

declare var AudioWorkletProcessor: {
    prototype: AudioWorkletProcessor;
    new(options?: AudioWorkletNodeOptions): AudioWorkletProcessor;
};

declare function registerProcessor(
    name: string,
    processorCtor: (new (
        options?: AudioWorkletNodeOptions
    ) => AudioWorkletProcessor) & {
        parameterDescriptors?: AudioParamDescriptor[];
    }
): undefined;

declare var currentTime: number;
declare var sampleRate: number;

class CustomSynthesizer extends AudioWorkletProcessor {
    generator: (time: number, freq: number) => number;
    phaseOffset: number;
    oldFrequency: number | null;

    constructor(options: AudioWorkletNodeOptions) {
        super(options);

        let generatorType = options.processorOptions[0];
        let useHarmonics = options.processorOptions[1];
        if (useHarmonics) {
            let harmonicMagnitudesGen = {
                "sine": (n: number) => n == 1 ? 1 : 0,
                "square": (n: number) => n % 2 == 1 ? 1 / n : 0,
                "sawtooth": (n: number) => Math.pow(-1, n + 1) / n,
                "triangle": (n: number) => n % 2 == 1 ? Math.pow(-1, (n - 1) / 2) / n / n : 0,
            }[generatorType];
            let harmonicMagnitudes = Array.from({length: 11}, (_, i) => harmonicMagnitudesGen(i + 1));

            this.generator = (time, freq) => {
                let s = 0.0;
                for (let h = 1; h <= 7 && freq * h <= sampleRate / 2; h++) {
                    s += harmonicMagnitudes[h - 1] * Math.sin(time * h * 2 * Math.PI);
                }
                return s;
            };
        } else {
            this.generator = {
                "sine": (t: number, _f: number) => Math.sin(t * 2 * Math.PI),
                "square": (t: number, _f: number) => Math.sign(t - 0.5),
                "sawtooth": (t: number, _f: number) => t * 2 - 1,
                "triangle": (t: number, _f: number) => 1 - 4 * Math.abs(t - 0.5),
            }[generatorType];
        }

        this.phaseOffset = 0;
        this.oldFrequency = null;
    }

    static get parameterDescriptors(): AudioParamDescriptor[] {
        return [
            {
                name: "frequency",
                defaultValue: 440,
                minValue: 1,
                maxValue: 20000,
                automationRate: "k-rate"
            },
            {
                name: "volume",
                defaultValue: 0.1,
                minValue: 0,
                maxValue: 1,
                automationRate: "a-rate"
            }
        ]
    }

    process(_inputs: any, outputs: Float32Array[][], params: Record<string, Float32Array>) {
        let freq = params["frequency"][0];
        let volumes = params["volume"];
        if (this.oldFrequency === null) {
            this.oldFrequency = freq;
        }
        if (freq !== this.oldFrequency) {
            this.phaseOffset = (this.phaseOffset + currentTime * (this.oldFrequency - freq)) % 1;
            this.oldFrequency = freq;
        }

        outputs[0].forEach(channelBuf => {
            for (let i = 0; i < channelBuf.length; i++) {
                let realTime = currentTime + i / sampleRate;
                let processedTime = (realTime * freq + this.phaseOffset) % 1;

                // We cannot normalize the maximum value on the whole sample since that breaks
                // down for small frequencies whose cycle doesn't fit in 128 samples.
                channelBuf[i] = this.generator(processedTime, freq);
                if (volumes.length == 1) {
                    channelBuf[i] *= volumes[0];
                } else {
                    channelBuf[i] *= volumes[i];
                }
            }
        })
        return true;
    }
}

registerProcessor("custom-synthesizer", CustomSynthesizer)
