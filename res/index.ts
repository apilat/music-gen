class Note {
    freq: number
    generator: string
    useHarmonics: boolean
    volume: number

    constructor(freq: number, generator: string, useHarmonics: boolean, volume: number) {
        this.freq = freq;
        this.generator = generator;
        this.useHarmonics = useHarmonics;
        this.volume = volume;
    }

    equals(other: Note): boolean {
        return this.freq === other.freq &&
            this.generator === other.generator &&
            this.useHarmonics === other.useHarmonics &&
            this.volume === other.volume;
    }

    generatorEquals(other: Note): boolean {
        return this.generator === other.generator &&
            this.useHarmonics === other.useHarmonics;
    }
}

function initializeControls(n: number) {
    currentNotes = [];
    for (let i = 0; i < n; i++) {
        currentNotes.push(null);

        let title = document.createElement("h3");
        title.innerText = `Synthesiser ${i}`;

        let form = document.createElement("form");
        form.id = `config${i}`;
        form.innerHTML = `
<input name="freq" type="number" value="440" min="1" max="20000" step="1">
<br>
Sine <input name="gen" value="sine" type="radio" checked>
Square <input name="gen" value="square" type="radio">
Sawtooth <input name="gen" value="sawtooth" type="radio">
Triangle <input name="gen" value="triangle" type="radio">
<br>
Perfect <input name="gen_type" value="perfect" type="radio" checked>
Harmonics <input name="gen_type" value="harmonics" type="radio">
<br>
On <input type="checkbox" name="on">
<br>
Volume <input name="volume" type="range" value="0.1" min="0" max="1" step="0.01">
        `;

        let container = document.getElementById(`main_col${i}`);
        container.appendChild(title);
        container.appendChild(form);

        form.addEventListener("input", () => {
            let note = new Note(
                parseFloat(form["freq"].value),
                form["gen"].value,
                form["gen_type"].value == "harmonics",
                parseFloat(form["volume"].value),
            );
            if (!form["on"].checked) {
                note = null;
            }
            playNote(note, i);
        });
        form.addEventListener("submit", evt => evt.preventDefault());
    }
}

let ctx = new AudioContext({
    latencyHint: 'interactive',
    sampleRate: 44100,
});
ctx.audioWorklet.addModule("res/processor.js");

let currentNotes: Array<[Note, AudioWorkletNode] | null>;
initializeControls(4);

function playNote(note: Note | null, playbackIndex: number) {
    let i = playbackIndex;
    if (note === null) {
        if (currentNotes[i] !== null) {
            currentNotes[i][1].disconnect();
            currentNotes[i] = null;
        }
    } else {
        if (currentNotes[i] !== null) {
            if (note.generatorEquals(currentNotes[i][0])) {
                let worklet = currentNotes[i][1];
                worklet.parameters.get("frequency").value = note.freq;
                worklet.parameters.get("volume").value = note.volume;
                return;
            } else {
                currentNotes[i][1].disconnect();
            }
        }
        let worklet = new AudioWorkletNode(ctx, 'custom-synthesizer', { processorOptions: [note.generator, note.useHarmonics] });
        worklet.parameters.get("frequency").value = note.freq;
        worklet.parameters.get("volume").value = note.volume;
        worklet.connect(ctx.destination);
        currentNotes[i] = [note, worklet];
    }
}
